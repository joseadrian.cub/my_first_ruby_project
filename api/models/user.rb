# frozen_string_literal: true

require 'sequel'
require './api/tasks/services'

module Models
  # User sequel model
  class User < Sequel::Model(Services[:database][:users])
    unrestrict_primary_key
  end
end
