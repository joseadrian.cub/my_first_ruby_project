# frozen_string_literal: true

require './api/models/user'

module DAO
  # DAO Class to manage users
  class UserDAO
    include Singleton

    def add(name, phone, code, roll)
      model.insert(
        name: name,
        phone: phone,
        code: code,
        roll: roll,
        activated: false
      )
    end

    def search(phone)
      model.find(phone: phone)
    end

    def all_actives
      model.where(activated: true).all
    end

    private

    def model
      Models::User
    end
  end
end
