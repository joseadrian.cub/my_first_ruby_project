# frozen_string_literal: true

require_relative 'endpoints/users'
require_relative '../rescues'

module Routes
  module V1
    # Entry point class for routes
    class API < Grape::API
      include Rescues
      version 'v1'
      format :json

      get :health do
        { status: 'ok' }
      end

      mount Endpoints::Users
    end
  end
end
