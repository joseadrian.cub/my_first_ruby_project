# frozen_string_literal: true

require './api/business/users'

module Routes
  module V1
    module Endpoints
      # Endpoints related to charges operations
      class Users < Grape::API
        resource :users do
          desc 'Add new user' do
            detail 'Add new user'
          end
          params do
            requires :name,
                     type: String,
                     desc: 'User personal full name',
                     allow_blank: false
            requires :phone,
                     type: String,
                     desc: 'User personal phone',
                     allow_blank: false
            requires :roll,
                     type: String,
                     desc: 'User roll',
                     allow_blank: false
          end
          post do
            {
              status: 'success',
              message: 'OK',
              code: 'OK',
              data: Business::Users.instance.add_user(params)
            }
          end

          desc 'Activated user' do
            detail 'Activated user'
          end
          params do
            requires :phone,
                     type: String,
                     desc: 'User personal phone',
                     allow_blank: false
            requires :code,
                     type: Integer,
                     desc: 'Activation code',
                     allow_blank: false
          end
          patch do
            {
              status: 'success',
              message: 'OK',
              code: 'OK',
              data: Business::Users.instance.activate_user(params[:phone], params[:code])
            }
          end

          desc 'Show users' do
            detail 'Show users'
          end
          get do
            {
              status: 'success',
              message: 'OK',
              code: 'OK',
              data: Business::Users.instance.show_user(headers['Authorization'])
            }
          end

          desc 'Login user' do
            detail 'Login user'
          end
          params do
            requires :phone,
                     type: String,
                     desc: 'User personal phone',
                     allow_blank: false
            requires :code,
                     type: Integer,
                     desc: 'Activation code',
                     allow_blank: false
          end
          post :login do
            {
              status: 'success',
              message: 'OK',
              code: 'OK',
              data: Business::Users.instance.login(params[:phone], params[:code])
            }
          end
        end
      end
    end
  end
end
