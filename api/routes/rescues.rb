# frozen_string_literal: true

# module rescues
module Rescues
  ACCESS_CONTROL_CORS = { 'Access-Control-Allow-Origin' => '*' }.freeze
  def self.included(base)
    base.class_eval do
      rescue_from RuntimeError do |e|
        error!({ status: 'fail', message: e.message }, 500, ACCESS_CONTROL_CORS)
      end
    end
  end
end
