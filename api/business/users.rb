# frozen_string_literal: true

require './api/dao/user_dao'
require './api/auth/authentication'
require './api/auth/authorization'

module Business
  # user class
  class Users
    include Singleton

    def add_user(params)
      name = params[:name]
      phone = params[:phone]
      roll = params[:roll]
      code = rand(1000..9999)

      user = DAO::UserDAO.instance.add(name, phone, code, roll)

      message = "#{code} es tu codigo de activacion"
      send_twilio_sms(phone, message)

      {
        id: user
      }
    end

    def activate_user(phone, code)
      user = DAO::UserDAO.instance.search(phone)
      raise 'Este usuario no esta registrado' unless user

      raise 'El codigo es incorrecto' unless user.code == code

      user.activated = true
      user.save

      'El usuario ha sido activado'
    end

    def login(phone, code)
      auth_user = AUTH::Authentication.new(phone, code)
      raise 'Incorrect credentials' unless auth_user.authenticate

      auth_user.generate_token
    end

    def show_user(token)
      raise 'Invalid token' unless token

      user = AUTH::Authorization.new(token).current_user
      raise 'Este usuario no esta registrado' unless user
      raise 'Este usuario no esta activo' unless user.activated == true
      raise 'Este usuario no tiene permisos para realizar esta accion' unless user.roll == 'Admin'

      DAO::UserDAO.instance.all_actives.map do |u|
        {
          name: u[:name], phone: u[:phone]
        }
      end
    end

    def send_twilio_sms(_phone, message)
      p message
      p message
    end
  end
end
