# frozen_string_literal: true

Sequel.migration do
  transaction

  up do
    #   extension :pg_triggers

    create_table(:users) do
      Serial   :id_user, primary_key: true
      String   :name, size: 20, null: false
      String   :phone, size: 20, unique: true, null: false
      Integer  :code, null: false
      String   :roll, size: 20, null: false
      Boolean  :activated, null: false, default: false

      DateTime :created_at, default: Sequel.function(:NOW)
      DateTime :updated_at, default: Sequel.function(:NOW)
    end

    #   pgt_updated_at(:riders,
    #                  :updated_at,
    #                  function_name: 'update_updated_at_column_at_riders_table',
    #                  trigger_name: :set_updated_at)
  end
end
