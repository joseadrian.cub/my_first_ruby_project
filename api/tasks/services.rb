# frozen_string_literal: true

require 'dry-container'
require 'dry-auto_inject'
require 'sequel'

require_relative 'service_config'

# Maintains the services in memory or creates them accordingly
class Services
  extend Dry::Container::Mixin

  register :database, memoize: true do
    configuration = ServiceConfig[:configuration]

    Sequel.connect(adapter: 'postgres',
                   host: configuration['DB_HOST'],
                   database: configuration['DB_DATABASE'],
                   user: configuration['DB_USER'],
                   password: configuration['DB_PASSWORD'])
  end
end

ServicesContainer = Dry::AutoInject(Services)
