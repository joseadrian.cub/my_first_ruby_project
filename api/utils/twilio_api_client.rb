# frozen_string_literal: true

require 'httparty'
require 'twilio-ruby'

# Twilio class
class Twilio
  include Singleton

  def send_sms(phone, message)
    account_sid = 'ACfd09e6e8a17089bf5d3b48e9c16e44d5' # Your Test Account SID from www.twilio.com/console/settings
    auth_token = 'your_api_key' # Your Test Auth Token from www.twilio.com/console/settings

    @client = Twilio::REST::Client.new account_sid, auth_token
    message = @client.messages.create(
      body: message,
      to: phone, # Replace with your phone number
      from: '+1 (415) 917-1595'
    )  # Use this Magic Number for creating SMS

    puts message.sid
  end
end
