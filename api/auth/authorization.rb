# frozen_string_literal: true

require './api/dao/user_dao'
require './api/auth/jwt'

module AUTH
  # Authorization Class
  class Authorization
    def initialize(token)
      # phone = phone[1..-1] if phone[0, 1] == '+'

      @token = token
      @token = token[7..-1] if token[0..6] == 'Bearer '
    end

    def current_user
      raise 'Invalid Token' unless @token

      phone = AUTH::Jwt.instance.decode(@token)['phone']
      user = DAO::UserDAO.instance.search(phone)

      raise 'No hay user' unless user

      user
    end
  end
end
