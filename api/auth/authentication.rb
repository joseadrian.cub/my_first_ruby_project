# frozen_string_literal: true

require './api/dao/user_dao'
require './api/auth/jwt'

module AUTH
  # For Authentication
  class Authentication
    def initialize(phone, code)
      @phone = phone
      @code = code
      @user = DAO::UserDAO.instance.search(@phone)
    end

    def authenticate
      @user && @user.code == @code
    end

    def generate_token
      AUTH::Jwt.instance.encode(phone: @user.phone)
    end
  end
end
