# frozen_string_literal: true

require './api/tasks/service_config'
require 'rake'

load 'Rakefile'
puts 'Running migrations'
Rake::Task['db:migrate'].invoke

# puts 'Running seeds'
# Rake::Task['db:seed'].invoke

require File.expand_path('application', __dir__)
run API::Root
