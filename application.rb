# frozen_string_literal: true

require 'grape'
require_relative './api/tasks/services'

require './api/routes/v1/routes'

module API
  # Grape API class. We will inherit from it in our future controllers.
  class Root < Grape::API
    format :json
    prefix :api
    content_type :json, 'application/json'

    mount Routes::V1::API
  end
end
